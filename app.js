var fs = require('fs');
var amqp = require('amqp');
var redis = require('redis');
var websocket = require("./websocket");
var mysql = require('mysql');

function getServiceInfo(service) {
	var redisconf = [];
	if (process.env.VCAP_SERVICES) {
		if(service == 'amqp'){
		    conf = JSON.parse(process.env.VCAP_SERVICES);
		    return conf['rabbitmq-2.4'][0].credentials.url;
		}
		if(service == 'redis'){
			redisconf = [conf['redis-2.2'][0].credentials.port, conf['redis-2.2'][0].credentials.host];
		    return redisconf;
		}
		if(service == 'mysql'){
			mysqlconf = {
				host: conf['mysql-5.1'][0].credentials.host, 
				port: conf['mysql-5.1'][0].credentials.port, 
				user: conf['mysql-5.1'][0].credentials.user,
				password: conf['mysql-5.1'][0].credentials.password,  
				database: 'd927acd49db384a859b48dfb7580bf35d'
			};
			return mysqlconf;
		}
	} else {
			if(service == 'amqp'){
			return "localhost";
		}
		if(service == 'redis'){
			redisconf = [6379, 'localhost'];
			return redisconf;
		}
		if(service == 'mysql'){
			mysqlconf ={
				host: 'localhost',
				user: 'root',
				password: '',
				database: 'vonix_pbx'
			}
			return mysqlconf;
		}
	}
}
var connection = amqp.createConnection({host: getServiceInfo('amqp')});
var redisClient = redis.createClient(getServiceInfo('redis')[0], getServiceInfo('redis')[1]);
var mysqlClient = mysql.createConnection(getServiceInfo('mysql'));
mysqlClient.connect(function(err){
	if(err){ console.log('MySQL error:' + err); }
});

// console.log(connection);
// console.log(redisClient);

// for handler use
testTrunk = {};

connection.on('ready', function(){
	testTrunk = connection.exchange('amq.topic', {autoDelete: true});
	testMessage();
});

// to publish messages every 2 seconds
function testMessage(){

	// READ FROM MYSQL
	mysqlClient.query(
		'SELECT * FROM trunks_simmulators',
		function selectCom(err, rows, fields){
			if (err) {
				console.log(err);
			}

			trunks = JSON.stringify({
				'total': rows[0].total,
				'qt_on': rows[0].qt_on,
				'qt_off': rows[0].qt_off
			});

			testTrunk.publish('trunk.list', JSON.stringify(trunks), {contentType: 'application/json', mandatory: true});
		}
	);

	// console.log('Handler ativo');
	setTimeout(testMessage, 2000);
};

// mediator action
connection.on('ready', function(){

    connection.queue('trunks.queue', function(q){
        q.bind('trunk.#');

        q.subscribe(function(message){

            var trunks = message;
            // console.log(trunks + ' from mediator');

            redisClient.on("error", function(err){
                console.log("Error: " + err);
            });

            redisClient.set("trunks", trunks);
        });
    });
});

console.log(websocket.connectSock(redisClient));