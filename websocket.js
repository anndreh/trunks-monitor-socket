var http = require('http');
var sockjs = require('sockjs');
var websocket;

websocket = {
    connectSock: function( redisClient ){
        // console.log(redisClient);
        var sock = sockjs.createServer();
        sock.on('connection', function(conn) {

            redisClient.on("error", function(err){
                console.log("Error: " + err);
            });

            var t = setInterval( function() {
                try{
                    conn._session.recv.didClose();
                } catch (x) {}
            }, 15000);

            setInterval( function(){
                redisClient.get("trunks", function (err, trunks ) {
                    conn.write(trunks);
                    // console.log(trunks);
                })
            }, 3000);

            conn.on('Close Socket', function() {
                console.log(" [.] close event received");
                clearInterval(t);
            });
        });

        var server = http.createServer();
        sock.installHandlers(server, {
            prefix: '/sockjs'
        });
        var port = process.env.VCAP_APP_PORT || 3000;
        console.log(' [*] Listening on 0.0.0.0:' + port);
        server.listen(port, '0.0.0.0');

        return null;
    }
}

module.exports = websocket;
//não consegui encontrar uma explicação (tutorial) boa sobre essa parte do websocket  