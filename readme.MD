# Trunks monitor server socket

## Requirements
* [RabbitMQ](http://www.rabbitmq.com)
* [Redis](http://redis.io)
* [NodeJS](http://nodejs.org)
* [MySQL](http://www.mysql.com)

## Preparing the environment
* Run the `npm install`

## The Servers
* Start RabbitMQ: `sudo rabbitmq-server`
* Start Redis: `sudo redis-server`

## In the /script folder
* Start the Handler, Mediator and Websocket: `node app.js`